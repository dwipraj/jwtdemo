﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using JWTDemo.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace JWTDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
		[HttpPost("token")]
		public ActionResult GetToken()
		{
			var symmentricSecurityKey = SecurityUtil.GetSymmetricSecurityKey();

			var signingCredentials = new SigningCredentials(symmentricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

			var claims = new List<Claim>();
			claims.Add(new Claim(ClaimTypes.Role, "Admin"));
			claims.Add(new Claim("UserName", "dwipraj"));

			var token = new JwtSecurityToken(
					issuer:"localhost",
					audience:"someone",
					expires:DateTime.Now.AddHours(1),
					signingCredentials:signingCredentials,
					claims:claims
				);

			return Ok(new JwtSecurityTokenHandler().WriteToken(token));
		}
    }
}