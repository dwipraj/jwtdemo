﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JWTDemo.Util
{
	public class SecurityUtil
	{
		public static SymmetricSecurityKey GetSymmetricSecurityKey()
		{
			string securityKey = "t|_|C#|_|tIy@{}|-|@I";

			var symmentricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

			return symmentricSecurityKey;
		}
	}
}
