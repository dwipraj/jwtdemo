﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWTDemo.ExceptionHandler
{
	public class CustomException : Exception
	{
		public CustomException(string message, Exception innerException) : base(message, innerException)
		{

		}
	}
}
